# @golemio/db-migrate-pg

__Forked from [github/db-migrate/pg](https://github.com/db-migrate/pg), originally developed by [Tobias Gurtzick](https://github.com/wzrdtales)__

Postgres driver for db-migrate

## Installation

```
yarn add db-migrate db-migrate-pg@npm:@golemio/db-migrate-pg
```
